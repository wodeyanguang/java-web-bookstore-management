package utils;

import javax.servlet.http.Cookie;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/7/6 15:15
 * @description：
 */
public class CookieUtils {
    public static Cookie findCookie(String name,Cookie[] cookies){
        if (name == null||cookies == null||cookies.length ==0){
            return null;
        }
        for (Cookie cookie :cookies){
            if (name.equals(cookie.getName())){
                return cookie;
            }
        }
        return  null;
    }
}
