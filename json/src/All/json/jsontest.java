package All.json;

import All.pojo.Person;
import com.google.gson.Gson;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/7/13 17:51
 * @description：
 */
public class jsontest {
    @Test
    public void test1(){
        Person yg = new Person(1, "YG");
        Gson gson = new Gson();
        String jsonString = gson.toJson(yg);
        Person fromJson = gson.fromJson(jsonString, Person.class);
        System.out.println(jsonString);
        System.out.println(fromJson);



        List arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add("arr");
        Gson gson1 = new Gson();
        String arrTojson = gson1.toJson(arrayList);
        System.out.println(arrTojson);

    }
}
