package WebAll.service.impl;

import WebAll.dao.UserDAO;
import WebAll.dao.impl.UserDAOimpl;
import WebAll.pojo.User;
import WebAll.service.UserService;
import com.sun.org.apache.bcel.internal.generic.NEW;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/6/3 12:39
 * @description：
 */
public class UserServiceimpl implements UserService {
    private UserDAO userDAO=new UserDAOimpl();
    @Override
    public void registUser(User user) {
            userDAO.saveUser(user);
    }

    @Override
    public User login(User user) {
        return userDAO.queryUserByUsernamePassword(user.getUsername(),user.getPassword());
    }

    @Override
    public boolean existUsername(String username) {
        if (userDAO.queryUserByUsername(username)==null){
            return false;
        }
        return true;
    }
}
