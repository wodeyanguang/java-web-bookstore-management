package WebAll.service.impl;

import WebAll.dao.BookDAO;
import WebAll.dao.impl.BookDAOimpl;
import WebAll.pojo.Book;
import WebAll.pojo.Page;
import WebAll.service.BookService;

import java.util.List;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/6/28 14:23
 * @description：
 */
public class BookServiceimpl implements BookService {
    private BookDAO bookDAO=new BookDAOimpl();
    @Override
    public void addBook(Book book) {
        bookDAO.addBook(book);
    }

    @Override
    public void deleteBookById(Integer id) {
        bookDAO.deleteBookById(id);
    }

    @Override
    public void updateBook(Book book) {
        bookDAO.updateBook(book);
    }

    @Override
    public Book queryBookById(Integer id) {
        return  bookDAO.queryBookById(id);
    }

    @Override
    public List<Book> queryBook() {
        return bookDAO.queryBook();
    }

    @Override
    public Page<Book> page(int pageNo, int pageSize) {
        Page<Book> page=new Page<>();
        page.setPageNo(pageNo);
        page.setPageSize(pageSize);
        Integer pageTotalCount=bookDAO.queryForPageTotalCount();
        page.setPageTotalCount(pageTotalCount);
        Integer pageTotal=pageTotalCount/pageSize;
        if (pageTotal%pageSize>0){
            pageTotal++;
        }
        page.setPageTotal(pageTotal);
        int begin=(page.getPageNo()-1)*pageSize;
        List<Book> items=bookDAO.queryForPageItems(begin,pageSize);
        page.setItems(items);
        return page;
    }
}
