package WebAll.service;

import WebAll.pojo.User;

public interface UserService {
    //注册
    public void registUser(User user);
    //登陆
    public User login(User user);
    //检查用户是否可用
    public boolean existUsername(String username);
}
