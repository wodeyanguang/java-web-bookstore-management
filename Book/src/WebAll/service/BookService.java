package WebAll.service;

import WebAll.pojo.Book;
import WebAll.pojo.Page;

import java.util.List;

public interface BookService {
    void addBook(Book book);

    void deleteBookById(Integer id);

    void updateBook(Book book);

    Book queryBookById(Integer id);

    List<Book> queryBook();

    Page<Book> page(int pageNo, int pageSize);
}
