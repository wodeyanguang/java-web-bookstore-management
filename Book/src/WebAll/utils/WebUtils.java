package WebAll.utils;

import WebAll.pojo.User;
import org.apache.commons.beanutils.BeanUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/6/24 17:01
 * @description： 将得到的请求的键值对给注入到对应的Bean中去。
 */
public class WebUtils {//因为不知道是什么类型的bean，所以使用泛型
    public static <T> T copyParamToBean(Map value, T bean){
        try {
            System.out.println("将数据注入到User之前"+bean);
            BeanUtils.populate(bean,value);//将请求中的键值对，直接存入到User中
            System.out.println("将数据注入到User之后"+bean);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bean;
    }
}
