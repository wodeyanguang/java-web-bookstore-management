package WebAll.web;

import WebAll.pojo.Book;
import WebAll.pojo.Page;
import WebAll.service.BookService;
import WebAll.service.impl.BookServiceimpl;
import WebAll.utils.WebUtils;
import com.sun.xml.internal.bind.v2.model.core.ID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class BookServlet extends BaseServlet {
    private BookService bookService=new BookServiceimpl();

    protected void page(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pageNo1 = request.getParameter("pageNo");
        int pageNo=0;
        if (pageNo1==null){
            pageNo=1;
        }else {
            pageNo= Integer.parseInt(pageNo1);
        }

        String pageSize1 = request.getParameter("pageSize");
        int pageSize=0;
        if (pageSize1==null){
            pageSize= Page.PAGE_SIZE;
        }else {
            pageSize= Integer.parseInt(pageSize1);
        }
        Page<Book> page=bookService.page(pageNo,pageSize);
        request.setAttribute("page",page);
        request.getRequestDispatcher("/pages/manager/book_manager.jsp").forward(request,response);//除了a标签，其他的都要以/打头

    }
    protected void add(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        //讲request获取到的键值对封装到Bean中
        Book book = WebUtils.copyParamToBean(request.getParameterMap(), new Book());
        //执行添加操作
        bookService.addBook(book);
        //重定向，这样可以防止重复添加
        response.sendRedirect(request.getContextPath()+"/manager/bookServlet?action=page&pageNo="+request.getParameter("pageNo"));
    }
    protected void delete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        bookService.deleteBookById(Integer.parseInt(id));
        response.sendRedirect(request.getContextPath()+"/manager/bookServlet?action=page&pageNo"+request.getParameter("pageNo"));
    }
    protected void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Book book = WebUtils.copyParamToBean(request.getParameterMap(), new Book());
        bookService.updateBook(book);
        response.sendRedirect(request.getContextPath()+"/manager/bookServlet?action=page&pageNo="+request.getParameter("pageNo"));
    }
    protected void getBook(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");//获取请求参数
        Book book = bookService.queryBookById(Integer.parseInt(id));
        request.setAttribute("book",book);//将book的参数存入request域中
        request.getRequestDispatcher("/pages/manager/book_edit.jsp").forward(request,response);
    }
    protected void list(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Book> books = bookService.queryBook();
        request.setAttribute("books",books);//给request域中设置参数
        request.getRequestDispatcher("/pages/manager/book_manager.jsp").forward(request,response);
    }
}
