package WebAll.web;

import WebAll.pojo.User;
import WebAll.service.impl.UserServiceimpl;
import WebAll.utils.WebUtils;
import org.apache.commons.beanutils.BeanUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY;

public class UserServlet extends BaseServlet{
    protected void logout(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getSession().invalidate();
        response.sendRedirect(request.getContextPath());
    }
    protected void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserServiceimpl userServiceimpl = new UserServiceimpl();

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        User loginUser = userServiceimpl.login(new User(null, username, password, null));

        if (loginUser==null){
            request.setAttribute("msg","用户名或者密码错误！");
            request.setAttribute("username",username);
            request.getRequestDispatcher("/pages/user/login.jsp").forward(request,response);
        }else {
            //将用户登陆信息保存到session域中
            request.getSession().setAttribute("user",loginUser);
            request.getRequestDispatcher("/pages/user/login_success.jsp").forward(request,response);
        }
    }
    protected void regist(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String token = (String) req.getSession().getAttribute(KAPTCHA_SESSION_KEY);
        req.getSession().removeAttribute(KAPTCHA_SESSION_KEY);
        //        1、获取请求的参数
        UserServiceimpl userServiceimpl = new UserServiceimpl();
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String email = req.getParameter("email");
        String code = req.getParameter("code");

            //将数据（请求的键值对）注入到JAVABean中
        User user = WebUtils.copyParamToBean(req.getParameterMap(),new User());


//        2、检查验证码是否正确
        if (token!=null&&token.equalsIgnoreCase(code)) {
            if (userServiceimpl.existUsername(username)) {
                //不可用
                System.out.println("用户名" + username + "不可用！");
                req.setAttribute("msg","用户名已存在！");
                req.setAttribute("username",username);
                req.setAttribute("email",email);
//                         跳回注册页面
                req.getRequestDispatcher("/pages/user/regist.jsp").forward(req, resp);
            } else {
                //正确
//                         3、检查用户名是否可用
//                         可用
//                         调用Sservice保存到数据库
                userServiceimpl.registUser(new User(null,username,password,email));
//                         跳到注册成功束面regist_ _success. html
                req.getRequestDispatcher("/pages/user/regist_success.jsp").forward(req, resp);
            }
        } else {
            //                不正确
            //                跳回注册页面.
            req.setAttribute("msg","验证码错误！");
            req.setAttribute("username",username);
            req.setAttribute("email",email);
            System.out.println("验证码[" + code + "]错误！");
            req.getRequestDispatcher("/pages/user/regist.jsp").forward(req, resp);
        }
    }

}
