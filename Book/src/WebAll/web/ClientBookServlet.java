package WebAll.web;

import WebAll.pojo.Book;
import WebAll.pojo.Page;
import WebAll.service.BookService;
import WebAll.service.impl.BookServiceimpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ClientBookServlet extends BaseServlet {
    private BookService bookService=new BookServiceimpl();

    protected void page(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pageNo1 = request.getParameter("pageNo");
        int pageNo=0;
        if (pageNo1==null){
            pageNo=1;
        }else {
            pageNo= Integer.parseInt(pageNo1);
        }

        String pageSize1 = request.getParameter("pageSize");
        int pageSize=0;
        if (pageSize1==null){
            pageSize= Page.PAGE_SIZE;
        }else {
            pageSize= Integer.parseInt(pageSize1);
        }
        Page<Book> page=bookService.page(pageNo,pageSize);
        request.setAttribute("page",page);
        request.getRequestDispatcher("/pages/client/index.jsp").forward(request,response);//除了a标签，其他的都要以/打头

    }
    protected void pageByPrice(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pageNo1 = request.getParameter("pageNo");
        int pageNo=0;
        if (pageNo1==null){
            pageNo=1;
        }else {
            pageNo= Integer.parseInt(pageNo1);
        }

        String pageSize1 = request.getParameter("pageSize");
        int pageSize=0;
        if (pageSize1==null){
            pageSize= Page.PAGE_SIZE;
        }else {
            pageSize= Integer.parseInt(pageSize1);
        }
        Page<Book> page=bookService.page(pageNo,pageSize);
        request.setAttribute("page",page);
        request.getRequestDispatcher("/pages/client/index.jsp").forward(request,response);//除了a标签，其他的都要以/打头

    }
}
