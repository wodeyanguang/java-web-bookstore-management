package WebAll.web;

import WebAll.pojo.User;
import WebAll.service.impl.UserServiceimpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserServiceimpl userServiceimpl = new UserServiceimpl();

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        User loginUser = userServiceimpl.login(new User(null, username, password, null));

        if (loginUser==null){
            request.setAttribute("msg","用户名或者密码错误！");
            request.setAttribute("username",username);
            request.getRequestDispatcher("/pages/user/login.jsp").forward(request,response);
        }else {
            request.getRequestDispatcher("/pages/user/login_success.jsp").forward(request,response);
        }
    }
}
