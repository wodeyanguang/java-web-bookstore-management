package WebAll.web;

import WebAll.pojo.Book;
import WebAll.pojo.Cart;
import WebAll.pojo.CartItem;
import WebAll.service.BookService;
import WebAll.service.impl.BookServiceimpl;
import com.sun.org.apache.bcel.internal.generic.NEW;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CartServlet extends BaseServlet {

    BookService bookService = new BookServiceimpl();

    protected void clearItem(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cart cart= (Cart) request.getSession().getAttribute("cart");
        if (cart!=null){
            cart.clear();
        }
        response.sendRedirect(request.getHeader("Referer"));
    }
    protected void updateCount(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id1 = request.getParameter("id");
        int id = 0;
        if (id1 == null) {
            id = 0;
        } else {
            id = Integer.parseInt(id1);
        }
        String count1 = request.getParameter("count");
        int count = 0;
        if (count1 == null) {
            count = 0;
        } else {
            count = Integer.parseInt(count1);
        }
        Cart cart = (Cart) request.getSession().getAttribute("cart");
        cart.updateCount(id,count);
        response.sendRedirect(request.getHeader("Referer"));
    }

    protected void deleteItem(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id1 = request.getParameter("id");
        int id = 0;
        if (id1 == null) {
            id = 0;
        } else {
            id = Integer.parseInt(id1);
        }
        Cart cart= (Cart) request.getSession().getAttribute("cart");

        if (cart!=null){
            cart.deleteItem(id);
        }
        response.sendRedirect(request.getHeader("Referer"));
    }
    protected void addItem(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id1 = request.getParameter("id");
        int id = 0;
        if (id1 == null) {
            id = 1;
        } else {
            id = Integer.parseInt(id1);
        }
        Book book = bookService.queryBookById(id);
        CartItem cartItem = new CartItem(book.getId(),book.getName(),1,book.getPrice(),book.getPrice());
        Cart cart = (Cart) request.getSession().getAttribute("cart");
        if (cart==null){
           cart = new Cart();
           request.getSession().setAttribute("cart",cart);
        }
        cart.addItem(cartItem);
        System.out.println(cart);
        request.getSession().setAttribute("lastName",cartItem.getName());
        response.sendRedirect(request.getHeader("Referer"));//首先利用请求头发送过去的referer，
        // 然后在点击点击事件访问服务器的时候获取一下发送过来的referer就可以了。而且这个referer的来的url地址是动态的。
    }

    }
