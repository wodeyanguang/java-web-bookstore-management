package WebAll.dao;

import WebAll.pojo.Book;

import java.util.List;

public interface BookDAO {
    public int addBook(Book book);

    public int deleteBookById(Integer id);

    public int updateBook(Book book);

    public Book queryBookById(Integer id);

    public List<Book> queryBook();


    Integer queryForPageTotalCount();

    List<Book> queryForPageItems(int begin, int pageSize);
}
