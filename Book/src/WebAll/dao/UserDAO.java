package WebAll.dao;

import WebAll.pojo.User;

public interface UserDAO {

    public User queryUserByUsername(String username);
    public User queryUserByUsernamePassword(String username,String password);
    public int saveUser(User user);
}
