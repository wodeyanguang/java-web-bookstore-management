package WebAll.dao.impl;

import WebAll.dao.UserDAO;
import WebAll.pojo.User;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/6/3 9:38
 * @description：
 */
public class UserDAOimpl extends BaseDAO implements UserDAO {
    @Override
    public User queryUserByUsername(String username) {
        String sql="select id,username,password,email from t_user WHERE username = ? ";
        return queryForOne(User.class,sql,username);
    }

    @Override
    public User queryUserByUsernamePassword(String username, String password) {
        String sql="select `id`,`username`,`password`,`email` from t_user where username=? and password=?";
        return queryForOne(User.class,sql,username,password);
    }

    @Override
    public int saveUser(User user) {
        String sql="insert into t_user(`username`,`password`,`email`) values(?,?,?)";
        return update(sql,user.getUsername(),user.getPassword(),user.getEmail());
    }
}
