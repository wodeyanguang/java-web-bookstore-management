package WebAll.test;

import WebAll.dao.UserDAO;
import WebAll.dao.impl.UserDAOimpl;
import WebAll.pojo.User;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserDAOTest {

    @Test
    public void queryUserByUsername() {
        UserDAO userDAO = new UserDAOimpl();
        if (userDAO.queryUserByUsername("admin")==null){
            System.out.println("用户名可用！");
        }else {
            System.out.println("用户名已存在！");
        }
    }


    @Test
    public void queryUserByUsernamePassword() {
        UserDAO userDAO = new UserDAOimpl();
        System.out.println(userDAO.queryUserByUsernamePassword("admin","admin"));
    }

    @Test
    public void saveUser() {
        UserDAOimpl userDAOimpl = new UserDAOimpl();
        System.out.println(userDAOimpl.saveUser(new User(null,"YG","123456","1825115398@qq.com")));
    }
}