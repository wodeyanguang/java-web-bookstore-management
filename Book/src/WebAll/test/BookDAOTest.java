package WebAll.test;

import WebAll.dao.BookDAO;
import WebAll.dao.impl.BookDAOimpl;
import WebAll.pojo.Book;
import WebAll.pojo.Page;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

public class BookDAOTest {
    private BookDAO bookDAO= new BookDAOimpl();
    @Test
    public void addBook() {
        bookDAO.addBook(new Book(null,"如何学好java","123",new BigDecimal(9999),99,0,null));
    }

    @Test
    public void deleteBookById() {
        bookDAO.deleteBookById(21);
    }

    @Test
    public void updateBook() {
        bookDAO.updateBook(new Book(21,"如何跟广哥学好java","广哥",new BigDecimal(9999),99,0,null));
    }

    @Test
    public void queryBookById() {
        System.out.println(bookDAO.queryBookById(20));
    }

    @Test
    public void queryBook() {
        for (Book book :bookDAO.queryBook()) {
            System.out.println(book);
        }
    }
    @Test
    public void  queryForPageTotalCount() {
        System.out.println(bookDAO.queryForPageTotalCount());
    }

    @Test
    public void queryForPageItems() {
        for (Book book : bookDAO.queryForPageItems(8, Page.PAGE_SIZE)) {
            System.out.println(book);
        }
    }
}