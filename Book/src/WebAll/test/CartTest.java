package WebAll.test;

import WebAll.pojo.Cart;
import WebAll.pojo.CartItem;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class CartTest {

    @Test
    public void addItem() {
        Cart cart1 = new Cart();
        cart1.addItem(new CartItem(1,"java入门",1,new BigDecimal(1000),new BigDecimal(1000)));
        cart1.addItem(new CartItem(1,"java入门",1,new BigDecimal(1000),new BigDecimal(1000)));
        cart1.addItem(new CartItem(2,"数据库入门",1,new BigDecimal(100),new BigDecimal(100)));
        System.out.println(cart1);
    }

    @Test
    public void deleteItem() {
        Cart cart2 = new Cart();
        cart2.addItem(new CartItem(1,"java入门",1,new BigDecimal(1000),new BigDecimal(1000)));
        cart2.addItem(new CartItem(1,"java入门",1,new BigDecimal(1000),new BigDecimal(1000)));
        cart2.addItem(new CartItem(2,"数据库入门",1,new BigDecimal(100),new BigDecimal(100)));
        cart2.deleteItem(2);
        System.out.println(cart2);
    }

    @Test
    public void clear() {
        Cart cart = new Cart();
        cart.addItem(new CartItem(1,"java入门",1,new BigDecimal(1000),new BigDecimal(1000)));
        cart.addItem(new CartItem(1,"java入门",1,new BigDecimal(1000),new BigDecimal(1000)));
        cart.addItem(new CartItem(2,"数据库入门",1,new BigDecimal(100),new BigDecimal(100)));
        cart.clear();
        System.out.println(cart);
    }

    @Test
    public void updateCount() {
        Cart cart = new Cart();
        cart.addItem(new CartItem(1,"java入门",1,new BigDecimal(1000),new BigDecimal(1000)));
        cart.addItem(new CartItem(1,"java入门",1,new BigDecimal(1000),new BigDecimal(1000)));
        cart.addItem(new CartItem(2,"数据库入门",1,new BigDecimal(100),new BigDecimal(100)));
        cart.updateCount(1,4);
        System.out.println(cart);
    }
}