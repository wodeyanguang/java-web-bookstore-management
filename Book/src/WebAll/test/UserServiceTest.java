package WebAll.test;

import WebAll.pojo.Page;
import WebAll.pojo.User;
import WebAll.service.BookService;
import WebAll.service.UserService;
import WebAll.service.impl.BookServiceimpl;
import WebAll.service.impl.UserServiceimpl;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserServiceTest {
    UserService userService=new UserServiceimpl();
    BookService bookService=new BookServiceimpl();

    @Test
    public void registUser() {
        userService.registUser(new User(null,"LCH","1234567","LCH@qq.com"));
        userService.registUser(new User(null,"LXG","123456","LXG@qq.com"));
    }

    @Test
    public void login() {
        System.out.println(userService.login(new User(null,"LCH","1234567","LCH@qq.com")));
    }

    @Test
    public void existUsername() {
        if (userService.existUsername("LCH")){
            System.out.println("用户名已存在！");
        }else {
            System.out.println("用户名可以使用！");
        }
    }
    @Test
    public void page(){
        System.out.println( bookService.page(1,Page.PAGE_SIZE));
    }
}