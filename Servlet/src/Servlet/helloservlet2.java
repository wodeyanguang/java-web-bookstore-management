package Servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/31 10:26
 * @description： F2,3,4外加一个ctrl+o还有上面的搜索图标就是我们常用的一些查看关系的方法
 */
public class helloservlet2 extends HttpServlet {
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        System.out.println("这是重写的init初始化方法");
    }

    public helloservlet2() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doget方法");
        ServletConfig servletConfig = getServletConfig();
        System.out.println(servletConfig.getInitParameter("url"));

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doPost");
    }
}
