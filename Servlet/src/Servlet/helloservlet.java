package Servlet;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/5/31 7:16
 * @description：
 */
public class helloservlet implements Servlet {
    public helloservlet() {
        System.out.println("构造器");
    }

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        System.out.println("init方法");
        System.out.println(servletConfig.getServletName());
        System.out.println(servletConfig.getInitParameter("url"));
        System.out.println(servletConfig.getServletContext());
    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        HttpServletRequest httpServletRequest= (HttpServletRequest) servletRequest;
        String method = httpServletRequest.getMethod();
        if ("GET".equalsIgnoreCase(method)){
            System.out.println("这是get方法");
        }else if ("POST".equalsIgnoreCase(method)){
            System.out.println("这是post方法");
        }
    }

    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public void destroy() {
        System.out.println("毁灭方法");
    }
}
