package Servlet;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class OutPutSteamServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        response.setCharacterEncoding("UTF-8");//设置服务器的编码格式
//        response.setHeader("Content-Type","text/html:charset=UTF-8");//设置浏览器的编码格式
        //最好的方法如下：
        response.setContentType("text/html:charset=UTF-8");

        PrintWriter writer = response.getWriter();
        writer.write("返回给客户端的字符串输出流");
    }
}
