package Servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Servlet2 extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        System.out.println(username+"柜台二的");
        //想要完成业务二，必须要业务一先盖章，不然就执行不了
        String key1 = (String) request.getAttribute("key1");
        if (key1=="柜台一的章")
        System.out.println(key1+"处理业务二的业务");
    }
}
