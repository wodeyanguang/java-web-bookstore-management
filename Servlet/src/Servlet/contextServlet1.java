package Servlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class contextServlet1 extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServletContext servletContext = getServletContext();
        //整个web项目当中，Context容器只有一个，所以其他的类也可以调用，只要你往里面set了值
        System.out.println(servletContext.getAttribute("key"));

        servletContext.setAttribute("key","abc");

        System.out.println(servletContext.getAttribute("key"));
        System.out.println(servletContext.getAttribute("key"));
        System.out.println(servletContext.getAttribute("key"));
        System.out.println(request.getRequestURI());
        System.out.println(request.getRequestURL());
    }
}
