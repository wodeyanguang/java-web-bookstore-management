package Servlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Servlet1 extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        System.out.println(username+"柜台一的");
        request.setAttribute("key1","柜台一的章");
        //请求下一步去哪里
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/Servlet2");//  /表示web工程下
        //去，并且把请求传给他
        requestDispatcher.forward(request,response);
        //也就是程序执行完servlet1就自动跳转去执行servlet2，然后传入请求跟响应
    }
}
