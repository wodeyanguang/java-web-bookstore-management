<%--
  Created by IntelliJ IDEA.
  User: 颜值军长
  Date: 2020/6/20
  Time: 10:30
  To change this template use File | Settings | File Templates.
--%>
<%--      <%! 声明代码脚本%>          --%>
<%--      <%= 表达式脚本 %>             --%>
<%--可以使用EL表达式替换表达式脚本${表达式}}--%>
<%--      <%代码脚本（Java语句）%>        --%>
<%--由JSTL取代--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <%@ include file="/index.jsp"%> <!-- (主要使用静态包含)静态包含（可以直接在此页面上加入其他页面）-->
    <jsp:include page="index.jsp"></jsp:include> <!--动态包含-->
    <jsp:forward page="index.jsp"></jsp:forward> <!--请求转发-->
</head>
<body>

</body>
</html>
