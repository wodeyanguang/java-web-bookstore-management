<%@ page import="com.sun.org.apache.xpath.internal.operations.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 颜值军长
  Date: 2020/6/23
  Time: 10:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%--1.遍历1到1e,输出
begin属性设置开始的索引
end属性设置结束的索引
var属性表示循环的变量(也是当前正在遍历到的数据)
for(inti=1;i<10;i++)
--%>
<c:forEach begin="1" end="10" var="i">
    ${i}
</c:forEach>

<%-- 2. 遍历bject数组
for (object item: arr)
items表示遍历的数据源(遍历的集合)
var表示当前遍历到的数据
--%>

<%
    request.setAttribute("arr",new java.lang.String[]{"123","456"});
%>
<%--类似增强for循环--%>
<c:forEach items="${requestScope.arr}" var="item">
    ${item}

</c:forEach>
</body>
</html>
