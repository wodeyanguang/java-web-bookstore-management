<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>尚硅谷会员注册页面</title>
			<%--静态包含base标签和css样式--%>
	<%@include file="/pages/common/head.jsp"%>

	<script type="text/javascript">

	$(function () {
		$("#code_img").click(function () {
			this.src = "${basePath}kaptcha.jsp?"+new Date();
		})

		$("#sub_btn").click(function () {
			//验证用户名:必须由字母，数字下划线组成，并且长度为5到12位
			let usernametext = $("#username").val();
			var usernamepatt=/^\w{2,12}$/;
			if (!usernamepatt.test(usernametext)){
				$(".errorMsg").text("用户名输入错误");
				return false;
			}
				$(".errorMsg").text("");
			//验证密码:必须由字母，数字下划线组成，并且长度为5到2位
			let passwordtext = $("#password").val();
			var passwordpatt=/^\w{5,12}$/;
			if (!passwordpatt.test(passwordtext)){
				$(".errorMsg").text("密码输入错误");
				return false;
			}
			$(".errorMsg").text("");

			//验证确认密码:和密码相同
			let $repwd = $("#repwd").val();
			if (!($repwd == passwordtext)){
				$(".errorMsg").text("密码不一致");
				return false;
			}
			$(".errorMsg").text("");

			//邮箱验证: xxxxx@xxx. com
			let emalitext = $("#email").val();
			var emailpatt = /^[a-z\d]+(\.[a-z\d]+)*@([\da-z](-[\da-z])?)+(\.{1,2}[a-z]+)+$/;
			if (!emailpatt.test(emalitext)){
				$(".errorMsg").text("邮箱输入错误");
				return false;
			}
				$(".errorMsg").text("");
			//验证码:现在只需要验证用户已输入。因为还没讲到服务器。验证码生成。
			let codetext = $("#code").val();
			if (codetext == null ||codetext == ""){
				$(".errorMsg").text("验证码未输入错误");
				return false;
			}
			$(".errorMsg").text("");
		})
	})

	</script>
	<style type="text/css">
		.login_form{
			height:420px;
			margin-top: 25px;
		}

	</style>
</head>
<body>
		<div id="login_header">
			<img class="logo_img" alt="" src="static/img/logo.gif" >
		</div>

			<div class="login_banner">

				<div id="l_content">
					<span class="login_word">欢迎注册</span>
				</div>

				<div id="content">
					<div class="login_form">
						<div class="login_box">
							<div class="tit">
								<h1>注册尚硅谷会员</h1>
								<span class="errorMsg">
<%--									<%=request.getAttribute("msg")==null?"":request.getAttribute("msg")%>--%>
									${requestScope.msg}
								</span>
							</div>
							<div class="form">
								<form action="userServlet" method="post">
									<input type="hidden" name="action" value="regist">
									<label>用户名称：</label>
									<input class="itxt" type="text" placeholder="请输入用户名"
										   value="${requestScope.username}"
										   autocomplete="off" tabindex="1" name="username" id="username" />
									<br />
									<br />
									<label>用户密码：</label>
									<input class="itxt" type="password" placeholder="请输入密码"
										   autocomplete="off" tabindex="1" name="password" id="password" />
									<br />
									<br />
									<label>确认密码：</label>
									<input class="itxt" type="password" placeholder="确认密码"
										   autocomplete="off" tabindex="1" name="repwd" id="repwd" />
									<br />
									<br />
									<label>电子邮件：</label>
									<input class="itxt" type="text" placeholder="请输入邮箱地址"
										   value="${requestScope.email}"
										   autocomplete="off" tabindex="1" name="email" id="email" />
									<br />
									<br />
									<label>验证码：</label>
									<input class="itxt" type="text" style="width: 80px;" id="code" name="code" />
									<img id="code_img" alt="" src="kaptcha.jsp" style="float: right; margin-right: 40px; width: 110px;height: 30px;">
									<br />
									<br />
									<input type="submit" value="注册" id="sub_btn" />

								</form>
							</div>

						</div>
					</div>
				</div>
			</div>
		<%@include file="/pages/common/footer.jsp"%>
</body>
</html>