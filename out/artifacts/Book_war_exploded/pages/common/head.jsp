<%--
  Created by IntelliJ IDEA.
  User: 颜值军长
  Date: 2020/6/24
  Time: 7:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String  basePath = request.getScheme()
        +"://"
        +request.getServerName()
        +":"
        +request.getServerPort()
        +request.getContextPath()
        +"/";
    pageContext.setAttribute("basePath",basePath);
%>
<base href="<%= basePath%>">
<!--相对路径都需要参考绝对路径然后加上自己的路径-->
<link type="text/css" rel="stylesheet" href="static/css/style.css" >
<script type="text/javascript" src="static/script/jquery-1.7.2.js"></script>
