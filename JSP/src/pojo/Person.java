package pojo;

import java.util.Arrays;
import java.util.HashMap;

/**
 * @author ：Yan Guang
 * @date ：Created in 2020/6/23 7:10
 * @description：
 */
public class Person {
    private String name;
    private String[] citys;
    private HashMap<String,Object>map;

    public Person() {
    }

    public Person(String name, String[] citys, HashMap<String, Object> map) {
        this.name = name;
        this.citys = citys;
        this.map = map;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getCitys() {
        return citys;
    }

    public void setCitys(String[] citys) {
        this.citys = citys;
    }

    public HashMap<String, Object> getMap() {
        return map;
    }

    public void setMap(HashMap<String, Object> map) {
        this.map = map;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", citys=" + Arrays.toString(citys) +
                ", map=" + map +
                '}';
    }
}
